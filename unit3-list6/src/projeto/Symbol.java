package projeto;

public class Symbol {
	
	public String symbol;
	public char value;
	
	public Symbol parent;
	public Symbol left;
	public Symbol right;
	
	@Override
	public String toString() {
		return "Symbol [symbol=" + symbol + ", value=" + value + "]";
	}
	
	public Symbol() {
		this.symbol = "";
		this.value = '@';
	}
	
	public Symbol(String symbol, char value) {
		this.symbol = symbol;
		this.value = value;
	}
}
