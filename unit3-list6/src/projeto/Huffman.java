package projeto;

import java.util.LinkedList;

public class Huffman {
	protected Tree tree;
	protected Tree treeS;
	LinkedList<Symbol> list;
	
	public Huffman(LinkedList<Node> list) {
		this.tree = new Tree(list);
		this.list = new LinkedList<>();
		tree.inOrder(false);
		
		this.list = insertSymbol(tree);
		System.out.println();
		System.out.println(this.list);
		System.out.println(this.list);
		
		this.treeS = new Tree(this.list, true);
		System.out.println();
		//System.out.println(this.treeS);
		treeS.inOrder(true);
	}    
	
	public LinkedList<Symbol> insertSymbol(Tree tree) {
		return insertSymbol(tree.root, "");
	}
	
	private LinkedList<Symbol> insertSymbol(Node node, String symbol) {
		LinkedList<Symbol> listAux = new LinkedList<>();
		LinkedList<Symbol> listAuxLeft = new LinkedList<>();
		LinkedList<Symbol> listAuxRight = new LinkedList<>();
		if(node != null) {
			if (node.isLeaf()) {
				Symbol aux = new Symbol(symbol, node.value);	
				listAux.add(aux);
				return listAux;
			} else {
				listAuxRight = insertSymbol(node.right, symbol + '1' );
				listAuxLeft = insertSymbol(node.left, symbol + '0');
			}	
		}
		listAuxRight.addAll(listAuxLeft);
		return listAuxRight; 
	}
}
