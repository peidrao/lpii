package projeto;

import java.util.LinkedList;

public class Tree {
	protected Node root;
	protected Symbol rootSymbol;

	public Tree(LinkedList<Node> list) {
		LinkedList<Node> listMin = new LinkedList<>();
		Node temp;
		int index = 0;
		while (list.size() > 0) {
			temp = list.get(0);

			for (int i = 0; i < list.size(); i++) {
				if (temp.quantity > list.get(i).quantity) {
					temp = list.get(i);
					index = i;
				}
			}
			listMin.addLast(temp);

			list.remove(index);
			index = 0;
		}

		while (!listMin.isEmpty()) {
			if (root != null) {
				if (root.quantity <= listMin.get(0).quantity) {
					insert(listMin.removeFirst());
				} else if (listMin.size() == 1)  {
					insert(listMin.removeFirst());
				} else {
					Node node = listMin.removeFirst();
					
					insert(node, listMin.removeFirst());
				}
			} else if (root == null) {
				insert(listMin.removeFirst());
			}
		}
	}
	
	public Tree(LinkedList<Symbol> listSymbol, boolean isSymbol) {
		isSymbol = true;
		this.rootSymbol = new Symbol();
		insertTreeSymbol(listSymbol);
	}

	public boolean insertTreeSymbol(LinkedList<Symbol> listSymbol) {
		return insertTreeSymbol(listSymbol, rootSymbol);
	}

	protected boolean insertTreeSymbol(LinkedList<Symbol> listSymbol, Symbol symbol) {
		LinkedList<Symbol> auxList = listSymbol;
		Symbol auxSymbol;
		//Symbol currentSymbol;
		//boolean inserted;
		while(!auxList.isEmpty()) {
	
			//inserted = false;
			auxSymbol = auxList.remove();
			
			Symbol currentSymbol = rootSymbol;		
			
			for(int i = 0; i < auxSymbol.symbol.length(); i++) {
	
				if(auxSymbol.symbol.charAt(i) == '0') {
					if(currentSymbol.left == null) {
						currentSymbol.left = new Symbol();
					}
					if(currentSymbol.left != null){
						currentSymbol = currentSymbol.left;
					}
				} else {
					if(currentSymbol.right == null) {
						currentSymbol.right = new Symbol();
					}
					if(currentSymbol.right != null){
						currentSymbol = currentSymbol.right;
					}
				}
				
				if(i == auxSymbol.symbol.length()-1) {
					
					currentSymbol.symbol = auxSymbol.symbol;
					
					currentSymbol.value = auxSymbol.value;
					//System.out.println("AQUi" + " Value: " + currentSymbol.value + " Symbol: " + currentSymbol.symbol);
					//System.out.println("AQUi" + currentSymbol.symbol);
				}
				
			} 
			
		//	auxSymbol = auxList.remove();
				
		}
		
		return false;
	}
	

	public boolean insert(Node node) {
		if (root == null) {
			root = node;
			root.quantity = 1;
			return true;
		} else {
			Node nodeUnion = new Node();
			Node nodeAux = root;
			nodeUnion.left = nodeAux;
			nodeUnion.right = node;
			nodeUnion.quantity = nodeAux.quantity + node.quantity;
			root = nodeUnion;
		}
		return false;
	}

	public boolean insert(Node node1, Node node2) {
		
		Node nodeUnion_1e2 = new Node();
		nodeUnion_1e2.left = node1;
		nodeUnion_1e2.right = node2;
		nodeUnion_1e2.quantity = node1.quantity + node2.quantity;

		Node nodeUnion_root_e_1e2 = new Node();
		nodeUnion_root_e_1e2.left = root;
		nodeUnion_root_e_1e2.right = nodeUnion_1e2;
		nodeUnion_root_e_1e2.quantity = root.quantity + nodeUnion_1e2.quantity;
		root = nodeUnion_root_e_1e2;

		return false;
	}

	public void inOrder(boolean isSymbol) {
		if(!isSymbol) {
			inOrder("", root, false);
		}else {
			inOrderSymbol( "", rootSymbol, false);
		}
	}

	private void inOrder(String prefix, Node n, boolean isLeft) {
		if (n != null) {
			inOrder(prefix + "    ", n.right, false);
			System.out.println(prefix + n.value + " [Qtd:" + n.quantity + "]");
			inOrder(prefix + "    ", n.left, true);
		}
	}
	
	private void inOrderSymbol(String prefix, Symbol s, boolean isLeft) {
		if (s != null) {
			inOrderSymbol(prefix + "    ", s.right, false);
			System.out.println(prefix + s.value + " [Symbol: " + s.symbol + "]");
			inOrderSymbol(prefix + "    ", s.left, true);
		}
	}

}
