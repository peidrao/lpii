package projeto;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

public class Main {

	public static void main(String[] args) throws IOException {
		LinkedList<Node> list = new LinkedList<>();
		// String filename = args[0];
		String filename = "teste.txt";

		try {
			FileReader readfile = new FileReader(filename);
			BufferedReader file = new BufferedReader(readfile);
			int readed = file.read();
			while (readed != -1) {
				char c = (char) readed;

				boolean contains = false;
				for (int i = 0; i < list.size(); i++) {
					if (list.get(i).value == c) {
						list.get(i).quantity++;
						contains = true;
						break;
					}
				}
				if (!contains) {
					Node node = new Node(c);
					list.add(node);
				}
				// System.out.println(c);
				readed = file.read();
			}
			file.close();
		} catch (FileNotFoundException e) {
			System.out.println("Something went wrong.\n" + e);
		}

		Huffman huffman = new Huffman(list);
	}

}
