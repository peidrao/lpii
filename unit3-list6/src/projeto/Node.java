package projeto;

public class Node {
	public char value;
	public int quantity;

	public Node parent;
	public Node left;
	public Node right;

	public Node() {
		this.quantity = 0;
		this.value = '@';
	}
	
	public Node(char value) {
		this.value = value;
		this.quantity = 1;
	}
	
	
	
	public Node(char  value, Node parent) {
		this.value = value;
		this.quantity = 1;
		this.parent = parent;
	}

	public boolean isLeaf() {
		return left == null && right == null;
	}

	public boolean hasLeft() {
		return left != null;
	}

	public boolean hasRight() {
		return right != null;
	}

	public void setChilds(Node left, Node right) {
		this.left = left;
		this.right = right;
	}

	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + value;
		return result;
	}

	public String toString() {
		return Integer.toString(value);
	}


}
