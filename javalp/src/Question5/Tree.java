package Question5;
import java.util.Stack;

public class Tree {
	public NodeN root;
	
	public void preOrder() {
		preOrder(root);
	}
	
	private void preOrder(NodeN root) {
		if (root == null) 
			return;
		Stack<NodeN> stack = new Stack<NodeN>();
		
		stack.push(root);
		while (!stack.isEmpty()) {
			NodeN current = stack.pop();
	
			System.out.print(current.toString() + " ");
		
			for (int i = 0; i < current.childs.size(); i++) {
				stack.add(current.childs.get(i));
			}
		}
	}

	public void binaryTree() {
		binaryTree(root);
	}
	
	
	private NodeN binaryTree(NodeN root) {
		NodeN tree = new NodeN(root.value);
		if (root.childs != null && root.childs.size() > 0) {
			tree.left = binaryTree(root.childs.get(0));
			NodeN current = tree.left;

			for (int i = 1; i < root.childs.size(); i++) {
				current.right = binaryTree(root.childs.get(i));
				current = current.right;
			}
		}
		return tree;
	}
}

