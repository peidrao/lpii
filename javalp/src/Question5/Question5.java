package Question5;

public class Question5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Tree tree = new Tree();
		
		tree.root = new NodeN(100);
		(tree.root.childs).add(new NodeN(1));
		(tree.root.childs).add(new NodeN(2));
		(tree.root.childs).add(new NodeN(3));
		(tree.root.childs).add(new NodeN(4));
		(tree.root.childs.get(0).childs).add(new NodeN(5));
	    (tree.root.childs.get(0).childs).add(new NodeN(10));
	    (tree.root.childs.get(1).childs).add(new NodeN(20));
	    (tree.root.childs.get(3).childs).add(new NodeN(30));
	    (tree.root.childs.get(3).childs).add(new NodeN(40));
	    (tree.root.childs.get(3).childs).add(new NodeN(50));
        
		tree.preOrder();
		System.out.print("\n");
		tree.binaryTree();
		tree.preOrder();
	}

}
