package Question3;
import java.io.*;

public class Question3 {
	public static void main(String[] args) {
		File directory = new File("/home/peidrao/image");	
		exploreFormatedWithLevel(directory);
	}
	
	public static void exploreFormatedWithLevel(File file) {
		exploreFormated(file, 1);
	}

	private static void exploreFormated(File file, int level) {
		String directoryFinal = "/home/peidrao/imageOut/";
		String extension = ".png";
		
		if (file.isDirectory()) {
			File[] files = file.listFiles();
			for (File f : files) {
					exploreFormated(f, level + 1);
					if(f.getName().endsWith(extension)) {
						File f2 = new File(directoryFinal+f.getName());
						f.renameTo(f2);
					}
				}
			}
		}
	}
	

