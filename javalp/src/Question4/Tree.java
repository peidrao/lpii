package Question4;

import java.util.Stack;


public class Tree {
	public NodeN root;
	
	public void preOrder() {
		preOrder(root);
	}

	private void preOrder(NodeN root) {
		if (root == null) 
			return;
		Stack<NodeN> stack = new Stack<NodeN>();
		
		stack.push(root);
		while (!stack.isEmpty()) {
			NodeN current = stack.pop();
	
			System.out.print(current.toString() + " ");
		
			for (int i = 0; i < current.childs.size(); i++) {
				stack.add(current.childs.get(i));
			}
		}
	}	
}
