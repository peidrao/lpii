package question3;

import java.util.Iterator;
import java.util.LinkedList;

public class IterableTree implements Iterable<Integer> {
	
	LinkedList<Node> list;

	public IterableTree(BinarySearchTree tree) {
		// Initializing stack
		list = new LinkedList<>();
		insertIterable(tree);
	}
	
	public void insertIterable(BinarySearchTree tree) {
		insertIterable(tree.root);
	}

	private void insertIterable(Node node) {
		if (node != null) {
			list.add(node);
			insertIterable(node.left);
			insertIterable(node.right);
		}
	}
	
	@Override
	public Iterator<Integer> iterator() {
		// TODO Auto-generated method stub
		for (Node value: list) {
			System.out.print(value + " ");
		}
		
		return null;
	}

}
