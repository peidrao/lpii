package question3;

public class Main {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BinarySearchTree tree = new BinarySearchTree();
		
		tree.insert(7);
		tree.insert(3);
		tree.insert(15);
		tree.insert(9);
		tree.insert(20);
		tree.inOrder();
		
		IterableTree iterate = new IterableTree(tree);
		System.out.println();
		iterate.iterator();
	}
}
