import java.util.ArrayList;
import java.util.List;

public class Element <T> {
	
	public static void main(String [] args) {
		
		Element<Integer> city1 = new Element<>(1);
		Element<Integer> city2 = new Element<>(2);
		Element<Integer> city3 = new Element<>(3);
		
		city1.getRepresentative();
		
	}

	protected T element;
	
	public List <Element<T>> representative;
	
	public Element(T element) {
		this.element = element;
		representative = new ArrayList<>();
		representative.add(this);
	}

	public Element<T> getRepresentative() {
		return representative.get(0);
	}

	public T getValue() {
		return element;
	}
	
	public String toString() {
		return element.toString();
	}
	
	public boolean inSameSet(Element<T> b) {
		return this.getRepresentative() == b.getRepresentative();
	}

	public void unionElements(Element<T> b) {
		Element<T> min = this.getRepresentative();
		for (Element<T> e : b.representative) {
			if (!this.representative.contains(e)) {
				this.representative.add(e);
				if (e.hashCode() < min.hashCode()) {
					min = e;
				}
			}
		}
		this.representative.remove(min);
		this.representative.add(0, min);
		b.representative = this.representative;
	}
}
