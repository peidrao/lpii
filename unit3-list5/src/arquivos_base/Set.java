//package aula07;
package arquivos_base;

public class Set {
	
	public static void main(String [] args) {
		Element<Integer> city1 = new Element<>(1);
		Element<Integer> city2 = new Element<>(2);
		Element<Integer> city3 = new Element<>(3);
		
		boolean v = Set.inSameSet(city1, city2);
	}

	public static <T> boolean inSameSet(Element<T> a, Element<T> b) {
		return a.getRepresentative() == b.getRepresentative();
	}

	public static <T> void unionElements(Element<T> a, Element<T> b) {
		Element<T> min = a.getRepresentative();
		for (Element<T> e : b.representative) {
			if (!a.representative.contains(e)) {
				a.representative.add(e);
				if (e.hashCode() < min.hashCode()) {
					min = e;
				}
			}
		}
		a.representative.remove(min);
		a.representative.add(0, min);
		b.representative = a.representative;
	}
}
