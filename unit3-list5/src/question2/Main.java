package question2;

public class Main {
	public static void main(String[] args) {
	
		BinarySearchTree tree = new BinarySearchTree();
		
		tree.insert(7);
		tree.insert(3);
		tree.insert(15);
		tree.insert(9);
		tree.insert(20);
		tree.inOrder();
		
		System.out.println();
		tree.preOrder();

	}
}
