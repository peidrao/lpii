package question2;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class IteratorTree implements Iterator<Integer> {

	Queue<Node> queue;

	public IteratorTree(BinarySearchTree tree) {
		// Initializing stack
		queue = new LinkedList<>();
		insertIterator(tree);
	}

	public void insertIterator(BinarySearchTree tree) {
		insertIterator(tree.root);
	}

	private void insertIterator(Node node) {
		if (node != null) {
			queue.add(node);
			insertIterator(node.left);
			insertIterator(node.right);
		}
	}

	@Override
	public boolean hasNext() {

		if (!queue.isEmpty()) {
			return true;
		}
		return false;

	}

	@Override
	public Integer next() {
		queue.remove();
		if (hasNext()) {
			return queue.element().value;
		}
		return null;
	}

	public Node curr() {
		return queue.peek();
	}

	public void iterate() {
		while (hasNext()) {
			System.out.print(" " + curr().value);
			next();
		}
	}
}
