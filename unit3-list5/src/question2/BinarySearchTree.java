package question2;


import java.util.LinkedList;
import java.util.Queue;
import java.util.Iterator;


public class BinarySearchTree implements Iterator<Integer>{
	
	protected Node root;
	Queue<Node> queue;
	

	public BinarySearchTree() {
		queue = new LinkedList<>();
	}
	
	
	public boolean insert(int value) {
		if (root == null) {
			root = new Node(value);
			return true;
		} else {
			return insert(root, value);
		}
	}

	private boolean insert(Node node, int value) {
		if (value > node.value) {
			if (node.hasRight()) {
				return insert(node.right, value);
			} else {
				node.right = new Node(value);
			}
		} else if (value < node.value) {
			if (node.hasLeft()) {
				return insert(node.left, value);
			} else {
				node.left = new Node(value);
			}
		} else {
			return false; // contains value
		}
		return true;
	}

	public boolean contains(int value) {
		return contains(root, value);
	}

	private boolean contains(Node node, int value) {
		if (node == null) {
			return false;
		} else {
			if (node.value == value) {
				return true;
			} else if (value > node.value) {
				return contains(node.right, value);
			} else if (value < node.value) {
				return contains(node.left, value);
			}
		}
		return false;
	}

	public boolean remove(int value) {
		return remove(root, null, value);
	}

	private boolean remove(Node node, Node parent, int value) {
		if (node == null) {
			return false;
		} else if (node.value == value) {
			if (node.isLeaf()) {
				updateChild(node, parent, null);
			} else if (node.hasLeft() && !node.hasRight()) {
				updateChild(node, parent, node.left);
			} else if (!node.hasLeft() && node.hasRight()) {
				updateChild(node, parent, node.right);
			} else {
				Node child = node.right;
				if (!child.hasLeft()) {
					child.left = node.left;
					updateChild(node, parent, child);
				} else {
					Node successor = removeSuccessor(child);
					successor.left = node.left;
					successor.right = node.right;
					updateChild(node, parent, successor);
				}
			}
		} else if (value > node.value) {
			return remove(node.right, node, value);
		} else if (value < node.value) {
			return remove(node.left, node, value);
		}
		return true;
	}

	private void updateChild(Node node, Node parent, Node child) {
		if (parent == null) {
			root = child;
		} else if (node.value > parent.value) {
			parent.right = child;
		} else if (node.value < parent.value) {
			parent.left = child;
		}
	}

	protected Node removeSuccessor(Node node) {
		if (!node.left.hasLeft()) {
			Node successor = node.left;
			node.left = successor.right;
			return successor;
		} else {
			return removeSuccessor(node.left);
		}
	}

	public void levelOrder() {
		LinkedList<Node> queue = new LinkedList<Node>();
		queue.addLast(root);
		while (!queue.isEmpty()) {
			Node current = queue.removeFirst();
			if (current != null) {
				System.out.println(root);
				queue.addLast(current.left);
				queue.addLast(current.right);
			}
		}
	}
	
	public void inOrder() {
		inOrder("", root, 1, false);
	}

	private void inOrder(String prefix, Node n, Integer level, boolean isLeft) {
		if (n != null) {

			inOrder(prefix + "    ", n.right, level + 1, false);
			System.out.println(prefix + n.value + " [Level:" + level + "]");
			inOrder(prefix + "    ", n.left, level + 1, true);

		}
	}

	@Override
	public boolean hasNext() {

		if (!queue.isEmpty()) {
			return true;
		}
		return false;

	}

	@Override
	public Integer next() {
		queue.remove();				//   5 | 8 | 1 | 9
		if (hasNext()) {
			return curr().value;
		}
		return null;
	}
	
	public Node curr() {
		return queue.peek();
	}
	
	
	protected void addQueue() {
		addQueue(root);
	}
	
	protected void addQueue(Node node) {
		if (node != null) {
			queue.add(node);
			addQueue(node.left);
			addQueue(node.right);
		}
	}

	public void preOrder() {
		addQueue();
		
		while (hasNext()) {
			System.out.print(curr().value + " ");
			next();
		}
	}

}
