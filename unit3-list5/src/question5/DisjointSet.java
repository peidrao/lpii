package question5;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class DisjointSet<T> implements DisjointInterface<T> {
	
	public static void main(String[] args) {
		DisjointSet<Integer> sets = new DisjointSet<Integer>();
		
		for(int i = 1;i <= 10;i++){
			sets.add(i);
		}
		
		System.out.println("Procurar conjunto");
		System.out.println(sets.findSet(5));
		System.out.println(sets.findSet(8));
		
		System.out.println("Fazer uni�o:");
		sets.union(5, 8);
		System.out.println(sets.findSet(5));
		System.out.println(sets.findSet(8));
		
		System.out.println("Fazer uni�o");
		System.out.println(sets.union(6, 5));
		System.out.println(sets.findSet(5));
		System.out.println(sets.findSet(6));
		
		System.out.println("Tamanho");
		System.out.println(sets.setSize(6));
		
		System.out.println("Testing atualiza��o de conjuntos:");
		System.out.println(sets.union(6, 5));
		System.out.println(sets.findSet(5));
		System.out.println(sets.findSet(6));
		System.out.println(sets.setSize(6));
		
		System.out.println("Teste contains:");
		System.out.println(sets.contains(5));
		System.out.println(sets.contains(11));
	}
	
	
	protected Map<T, Node> elements;
	protected int setCount;
	 
	public DisjointSet() {
		// TODO Auto-generated constructor stub
		elements = new HashMap<T, Node>();
		setCount = 0;							// atribut que armazena a quantidade de elementos adicionados
	}
	
	
	@Override
	public boolean add(T element) {
		if(contains(element))
			return false;
		else{
			Node newNode = new Node(element);
			elements.put(element, newNode);
			setCount++;
			return true;
		}
	}

	@Override
	public T union(T x, T y) {
		if(contains(x) && contains(y)){
			T xRep = findSet(x);
			T yRep = findSet(y);
			if(!xRep.equals(yRep))
				return link(xRep, yRep);
			else
				return xRep;
		}
		else
			return null;
	}
	
	private T link(T x, T y){
		Node xNode = elements.get(x);
		Node yNode = elements.get(y);
		
		setCount--;
		if(xNode.rank > yNode.rank){
			yNode.parent = xNode;
			xNode.setSize += yNode.setSize;
			return x;
		}
		else{
			xNode.parent = yNode;
			yNode.setSize += xNode.setSize;
			if(xNode.rank == yNode.rank)
				yNode.rank++;
			return y;
		}
		
	}

	@Override
	public T findSet(T element) {
		// TODO Auto-generated method stub
		T representative = null;
		
		if(contains(element)){
			Node curr = elements.get(element);
			Stack<Node> nodes = new Stack<>();
			while(curr.parent != curr){
				nodes.push(curr);
				curr = curr.parent;
			}
			representative = curr.element;
			while(!nodes.isEmpty()){
				Node top = nodes.pop();
				top.parent = curr;
			}
		}
		return representative;
	}

	@Override
	public boolean contains(T element) {
		return elements.containsKey(element);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return elements.size();
	}

	@Override
	public int setCount() {
		return setCount;
	}

	@Override
	public int setSize(T element) {
		// TODO Auto-generated method stub
		if(contains(element))
			return elements.get(findSet(element)).setSize;
		else
			return 0;
	}

	
	private class Node{
		T element;
		int rank;
		Node parent;
		int setSize;
		
		public Node(T element){
			this.element = element;
			rank = 0;
			parent = this;
			setSize = 1;
		}
	}

}