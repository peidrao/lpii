package question5;

public interface DisjointInterface<T> {
	
	boolean add(T element);	
	T union(T x, T y);		
	T findSet(T element);
	boolean contains(T element);	
	int size();	
	int setCount();
	int setSize(T element);
}
