package question1;
public class BinarySearchTree <T extends Comparable<T>> {

	protected Node<T> root;
	
	public boolean insert(T value) {
		if(root == null) {
			root = new Node<T>(value);
			return true;
		} else {
			return insert(root, value);
		}
	}
	
	private boolean insert(Node<T> node, T value) {
		if(value.compareTo(node.value)> 0) { // Precisa ver essa ordem, se está certo.
			if(node.hasRight()) {
				return insert(node.right, value);
			} else {
				node.right = new Node<T>(value);
			}
		} else if(value.compareTo(node.value)<0) { // Precisa ver essa ordem, se está certo. 
			if(node.hasLeft()) {
				return insert(node.left, value);
			} else {
				node.left = new Node<T>(value);
			}
		} else {
			return false; // contains value
		}
		return true;
	}
	
	public boolean contains(T value) {
		return contains(root, value);
	}
	
	private boolean contains(Node<T> node, T value) {
		if(node == null) {
			return false;
		} else {
			if(node.value.compareTo(value) == 0) {
				return true;
			} else if(value.compareTo(node.value) > 0) {
				return contains(node.right, value);
			} else if(value.compareTo(node.value) < 0){
				return contains(node.left, value);
			}
		}
		return false;
	}
	
	
	public boolean remove(T value) {
		return remove(root, null, value);
	}
	
	private boolean remove(Node<T> node, Node<T> parent, T value) {
		if(node == null) {
			return false;
		} else if(node.value.compareTo(value) == 0) {
			if(node.isLeaf()) {
				updateChild(node, parent, null);
			} else if(node.hasLeft() && !node.hasRight()) {
				updateChild(node, parent, node.left);
			} else if(!node.hasLeft() && node.hasRight()) {
				updateChild(node, parent, node.right);
			} else {
				Node<T> child = node.right;
				if(!child.hasLeft()) {
					child.left = node.left;
					updateChild(node, parent, child);
				} else {
					Node<T> successor = removeSuccessor(child);
					successor.left = node.left;
					successor.right = node.right;
					updateChild(node, parent, successor);
				}
			}
		} else if(value.compareTo(node.value) > 1) {
			return remove(node.right, node, value);
		} else if(value.compareTo(node.value) < -1 ) {
			return remove(node.left, node, value);
		}
		return true;
	}
	
	
	
	private void updateChild(Node<T> node, Node<T> parent, Node<T> child) {
		if(parent == null) {
			root = child;
		} else if(node.value.compareTo(parent.value) > 1) {
			parent.right = child;
		} else if(node.value.compareTo(parent.value) < -1) {
			parent.left = child;
		}
	}

	protected Node<T> removeSuccessor(Node<T> node) {
		if(!node.left.hasLeft()) {
			Node<T> successor  = node.left;
			node.left = successor.right;
			return successor;
		} else {
			return removeSuccessor(node.left);
		}
	}
	
	public void inOrder() {
		inOrder("", root, 1, false);
	}

	private void inOrder(String prefix, Node<T> n, Integer level, boolean isLeft) {
		if (n != null) {
			inOrder(prefix + "    ", n.right, level + 1, false);
			System.out.println(prefix + n.value + " [Level:" + level + "]");
			inOrder(prefix + "    ", n.left, level + 1, true);

		}
	}
}