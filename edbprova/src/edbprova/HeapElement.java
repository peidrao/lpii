package edbprova;

public class HeapElement {
	public int data;
	public int count;

	public HeapElement(int data, int count) {
		this.data = data;
		this.count = count;
	}
}
