package BinarySearchTree;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BinarySearchTree tree = new BinarySearchTree();

		// ler primeiro arquivo com os números que devem ser inseridos na árvore:
		//String nomeArquivo = "teste.txt"; // nome do arquivo de dados
		String nomeArquivo = args[0]; // nome do arquivo de dados
		String linha = new String();
		File arq = new File(nomeArquivo);

		// inserção dos valores presentes no arq1.txt
		if (arq.exists()) {// checa se existe o arquivo em questao
			try {
				FileReader leitorDeArquivo = new FileReader(nomeArquivo);
				BufferedReader bufferDeArquivo = new BufferedReader(leitorDeArquivo);

				while (true) {
					linha = bufferDeArquivo.readLine();// lendo linhas do arquivo
					String[] linhaQuebrada = linha.split(" ");
					if (linha == null) { // verificar se a linha do arquivo está nula
						break;
					}
					for (int i = 0; i < linhaQuebrada.length; i++) {
						tree.insert(Integer.parseInt(linhaQuebrada[i]));
					}
					System.out.println(tree);
				}
			} catch (Exception e) {
			}
		}
		String nomeArquivo2 = args[1]; // nome do arquivo de operacoes
		//String nomeArquivo2 = "operacoes.txt"; // nome do arquivo de operacoes
		String linha2 = new String();
		File arq2 = new File(nomeArquivo2);

		if (arq2.exists()) {
			try {
				FileReader leitorDeArquivo = new FileReader(nomeArquivo2);
				BufferedReader bufferDeArquivo = new BufferedReader(leitorDeArquivo);

				while (true) {
					linha2 = bufferDeArquivo.readLine();// lendo linhas do arquivo
					String[] linhaQuebrada2 = linha2.split(" ");
					if (linha2 == null) // verificar se a linha do arquivo está nula
						break;

					if (linhaQuebrada2.length == 2) { // se na linha tiver dois elementos
						if (linhaQuebrada2[0].equals("REMOVER")) {// se o primeiro for remover
							tree.remove(Integer.parseInt(linhaQuebrada2[1]));
							System.out.println("Valor removido: " + linhaQuebrada2[1] + "\n");
							System.out.println(tree);
						} else if (linhaQuebrada2[0].equals("POSICAO")) { // se for posicao
							System.out.println("Percorrendo a ABB de forma simetrica o elemento "
									+ Integer.parseInt(linhaQuebrada2[1]) + " encontra-se na posição "
									+ tree.index(Integer.parseInt(linhaQuebrada2[1])));
						} else if (linhaQuebrada2[0].equals("INSIRA")) { // se for insira
							if(!tree.insert(Integer.parseInt(linhaQuebrada2[1]))) {
								System.out.println("Elemento " + Integer.parseInt(linhaQuebrada2[1]) + "já está adicionando na árvore.");
							}
							else {
								System.out.println("Inserindo o elemento " + Integer.parseInt(linhaQuebrada2[1]));
								tree.insert(Integer.parseInt(linhaQuebrada2[1]));
							}
							
							System.out.println(tree);
						} else if (linhaQuebrada2[0].equals("ENESIMO")) { // se for enesimo
							System.out.println("O elemento da posição " + Integer.parseInt(linhaQuebrada2[1])
									+ " percorrendo a árvore de forma simétrica é: "
									+ tree.nthElement(Integer.parseInt(linhaQuebrada2[1])));
						}
					} else if (linhaQuebrada2.length == 1) { // Se só tiver um elemento
						if (linhaQuebrada2[0].equals("MEDIANA")) {// se for mediana
							System.out.println("Mediana da árvore: " + tree.median());
						} else if (linhaQuebrada2[0].equals("CHEIA")) {// se for cheia
							System.out.println("A árvore é cheia: " + tree.isFull());
						} else if (linhaQuebrada2[0].equals("COMPLETA")) {// se for completa
							System.out.println("A árvore é completa: " + tree.isComplete());
						} else if (linhaQuebrada2[0].equals("IMPRIMA")) {// se for imprima
							System.out.println("Impressão da sequência de vistação da árvore: \n" + tree.toString());
						}

					}

				}

			} catch (Exception e) {
			}
		}

	}

}
