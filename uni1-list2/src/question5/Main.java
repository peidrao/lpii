package question5;

public class Main {

	public static void main(String[] args) {
		TreeV3 tree = new TreeV3();
		tree.insert(10);
		tree.insert(20);
		tree.insert(30);
		tree.insert(40);
		tree.inOrder();
		
	
		System.out.print("\n");
		tree.remove(40);
		tree.inOrder();
		System.out.print("\n");

	}

}
