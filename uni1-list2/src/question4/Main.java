package question4;

public class Main {

	public static void main(String[] args) {
		Tree tree = new Tree();
		tree.insert(5);
		tree.insert(6);
		tree.insert(7);
		tree.insert(8);
		
		tree.inOrder();
		System.out.print("\n");
		tree.levelOrder();
		System.out.print("\n");
		
		tree.removeSucessor(5);
		System.out.print("\n");
		tree.inOrder();
	}

}
