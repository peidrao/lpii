package aluno20180120844;

public class Tree {
	public Node root;

	public boolean insert(int value) {
		if (root == null) {
			root = new Node(value);
			return true;
		} else {
			return insert(root, value);
		}
	}

	private boolean insert(Node node, int value) {
		if (value > node.value) {
			if (node.hasRight()) {
				return insert(node.right, value);
			} else {
				node.right = new Node(value);
			}
		} else if (value < node.value) {
			if (node.hasLeft()) {
				return insert(node.left, value);
			} else {
				node.left = new Node(value);
			}
		} else {
			return false; // contains value
		}
		return true;
	}
	
	public boolean equals(Node tree1) {
		return equals(root, tree1);
	}
	
	public boolean equals(Node tree, Node tree1) {
		if(tree1 == null && tree == null) {
			return true;
		}
		if(tree1 == null || tree == null) return false;
		if (tree.value != tree1.value) return false;
		if (equals(tree.left, tree1.left) == false) return false;
		if (equals(tree.right, tree1.right) == false) return false;
		return true;
	}
	
}
	
	
	

