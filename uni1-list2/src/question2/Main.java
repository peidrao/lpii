package question2;


public class Main {

	public static void main(String[] args) {
		Tree tree = new Tree();
		TreeV1 tree1 = new TreeV1();
		
		int [] values = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15, 16, 17, 18, 19, 20};
		
		for (int i : values) {
			tree.insert(i);
			tree1.insert(i);
		}
		
		tree.inOrder();
		System.out.print("\n");
		tree1.inOrder();
		System.out.print("\n");

		boolean isTrue1 =  tree1.equals(tree.root);		
		System.out.print((isTrue1) ? "True" : "False");
	}

}
