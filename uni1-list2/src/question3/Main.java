package question3;
public class Main {

	public static void main(String[] args) {
		Tree tree = new Tree();
		TreeV2 treev2 =  new TreeV2();
		
		int  values []  = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};

	
		for (int i : values) {
			//tree.insert(i);
			treev2.insert(i);
		}
		
		tree.inOrder();
		System.out.print("\n");
		treev2.inOrder();
		System.out.print("\n");
		
		System.out.print("\n");
		boolean isTrue =  treev2.equals(tree.root);		
		System.out.print((isTrue) ? "True" : "False");
	}

}
