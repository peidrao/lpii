package Question1;

public class Main {

	public static void main(String[] args) {

		PriorityTree tree = new PriorityTree();

		tree.insert(7);
		tree.insert(5);
		tree.insert(24);
		tree.insert(88);
		tree.insert(22);
		tree.insert(33);
		tree.insert(11);
		tree.insert(9);
		tree.insert(3);
		tree.insert(2);
		tree.insert(10);
		tree.insert(4);

		System.out.println("------------------------------------- ");
		tree.inOrder();
		System.out.println("------------------------------------- ");

		System.out.println("SIZE: " + tree.size());

		System.out.println("GET: " + tree.get());

		System.out.println("DELETE: " + tree.remove());

		System.out.println("SIZE: " + tree.size());

		tree.update(24, 99);
		System.out.println("------------------------------------- ");
		tree.inOrder();
		System.out.println("------------------------------------- ");

		System.out.println("SIZE: " + tree.size());

		System.out.println("GET: " + tree.get());

		System.out.println("DELETE: " + tree.remove());

		System.out.println("SIZE: " + tree.size());

		System.out.println("------------------------------------- ");
		tree.inOrder();
		System.out.println("------------------------------------- ");
	}

}
