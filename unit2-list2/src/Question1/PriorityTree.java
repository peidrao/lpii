package Question1;

import java.util.LinkedList;
import professor.CompleteBinaryTree;
import professor.Node;
import professor.PriorityQueue;

public class PriorityTree extends CompleteBinaryTree implements PriorityQueue {

	protected int size;

	@Override
	public void update(int current, int newValue) {
		Node node;
		node = getNode(current);

		node.value = newValue;

		if (current < newValue) {
			up(node);
		} else {
			down(node);
		}

	}

	@Override
	public boolean insert(int value) {

		if (root == null) {
			root = new Node(value, null);
			size = 1;
			root.index = size;
			return true;
		} else {
			LinkedList<Node> queue = new LinkedList<>();
			queue.addLast(root);

			boolean inserted = false;

			while (!queue.isEmpty() && !inserted) {

				Node current = queue.removeFirst();

				if (!current.hasLeft()) {
					current.left = new Node(value, current);
					size += 1;
					current.left.index = size;

					up(current.left);

					inserted = true;

				} else if (!current.hasRight()) {
					current.right = new Node(value, current);
					size += 1;
					current.right.index = size;

					up(current.right);

					inserted = true;

				} else {
					if (current.hasLeft()) {
						queue.add(current.left);
					}
					if (current.hasRight()) {
						queue.add(current.right);
					}
				}
			}

			return inserted;
		}
	}

	private void up(Node node) {
		Node child = node;
		Node parent = child.parent;
		if (child.parent != null) {
			if (child.value > parent.value) {
				swap(child, parent);
				up(parent);
			}
		}
	}

	private void down(Node node) {
		Node parent = node;
		Node childRigth = parent.right;
		Node childLeft = parent.left;

		if (parent.index < size) {
			if (parent.hasRight()) {
				if (parent.hasLeft()) {
					if (childRigth.value >= childLeft.value) {
						if (childRigth.value > parent.value) {
							swap(childRigth, parent);
							down(childRigth);
						}
					}
					if (childLeft.value > parent.value) {
						swap(childLeft, parent);
						down(childLeft);
					}

				} else {
					if (childRigth.value > parent.value) {
						swap(childRigth, parent);
						down(childRigth);
					}
				}
			} else if (parent.hasLeft()) {
				if (childLeft.value > parent.value) {
					swap(childLeft, parent);
					down(childLeft);

				}
			}
		}
	}

	private void swap(Node child, Node parent) {
		int aux = child.value;
		child.value = parent.value;
		parent.value = aux;
	}

	@Override
	public int remove() {
		if (root == null) {
			return -111;
		}
		int removed = root.value;

		root.value = removeLast();

		size--;
		down(root);

		return removed;
	}

	protected int removeLast() {
		return removeLast(root, false);
	}

	private int removeLast(Node node, boolean isLeft) {

		if (node != null) {
			int removed, removedRight, removedLeft;

			if (node.index == size) {
				removed = node.value;

				if (node.parent.right == node) {
					node.parent.right = null;
				} else {
					node.parent.left = null;
				}

				return removed;
			} else {
				removedRight = removeLast(node.right, false);
				removedLeft = removeLast(node.left, true);
				if (removedLeft != -111) {
					return removedLeft;
				} else {
					return removedRight;
				}
			}
		}

		return -111;
	}

	@Override
	public int get() {
		return root.value;
	}

	protected Node getNode(int current) {
		return getNode(root, current);
	}

	protected Node getNode(Node node, int current) {
		if (node != null) {
			if (node.value == current) {
				return node;
			} else {
				return getNode(node.right, current) != null ? getNode(node.right, current)
						: getNode(node.left, current);
			}
		}
		return null;
	}

	@Override
	public int size() {
		return size;
	}

	public void inOrder() {
		inOrder("", root, 1, false);
	}

	private void inOrder(String prefix, Node n, Integer level, boolean isLeft) {
		if (n != null) {

			inOrder(prefix + "    ", n.right, level + 1, false);
			System.out.println(prefix + n.value + " [Level:" + level + "]");
			inOrder(prefix + "    ", n.left, level + 1, true);

		}
	}

}
