package Question2;

public class Solutions {

	public double quality;
	public String text;
	public int index;

	public Solutions parent;
	public Solutions left;
	public Solutions right;

	public Solutions(int quality) {
		this.quality = quality;
		this.text = "null";
	}
	
	public Solutions(int quality, String text) {
		this.quality = quality;
		this.text = text;
	}
	
	public Solutions(int quality, String text, Solutions parent) {
		this.quality = quality;
		this.text = text;
		this.parent = parent;
	}

	public boolean isLeaf() {
		return left == null && right == null;
	}

	public boolean hasLeft() {
		return left != null;
	}

	public boolean hasRight() {
		return right != null;
	}

	public void setChilds(Solutions left, Solutions right) {
		this.left = left;
		this.right = right;
	}

	/*public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + quality;
		return result;
	}*/

	public String toString() {
		return " Quality:  " + Double.toString(quality) + " / Text: " + text + "\n";
	}


}
