package Question2;

public class Main {

	public static void main(String[] args) {
		
		SolutionsList tree = new SolutionsList(5);
		
		tree.insert(7, "jose");
		tree.insert(1, "andre");
		tree.insert(24, "pereira");
		tree.insert(88, "da");
		tree.insert(22, "silva");
		tree.insert(33, "neto");
		tree.insert(11, "pedro");
		tree.insert(9, "victor");
		tree.insert(3, "da");
		tree.insert(2, "fonseca");
		
		System.out.println("------------------------------------- ");
		tree.inOrder();
		System.out.println("------------------------------------- ");
		
		System.out.println("GET: " + tree.get());
		//System.out.println("DELETE: " + tree.remove());
		
		tree.update(24, 3.5);
		
		System.out.println("------------------------------------- ");
		tree.inOrder();
		System.out.println("------------------------------------- ");
		
		System.out.println("GET: " + tree.get());
		
		System.out.println("------------------------------------- ");
	}

}
