package Question2;

import java.util.LinkedList;
//import professor.PriorityQueue;

public class SolutionsList extends Solutions /*implements PriorityQueue*/{
	
	protected int sizeMax, size;
	protected Solutions root;
	
	
	public SolutionsList(int sizeMax) {
		super(sizeMax);
		this.sizeMax = sizeMax;
		this.root = null;
		
	}
	
	
	public void update(double current, double newValue) {
		Solutions solutions;
		solutions = getSolutions(current);
		
		solutions.quality = newValue;
		
		if(current > newValue) {
			up(solutions);
		}else {
			down(solutions);
		}
		
	}

	public boolean insert(int value, String text) {
		
		if(root == null) {
			root = new Solutions(value, text);
			size = 1;
			root.index = size;
			return true;
		} else if(size < sizeMax){
			LinkedList <Solutions> queue = new LinkedList<>();
			queue.addLast(root);
			
			boolean inserted = false;
			
			while(!queue.isEmpty() && !inserted) {
				
				Solutions current = queue.removeFirst();
				
				if(!current.hasLeft()) {
					current.left = new Solutions(value, text,  current);
					size += 1;
					current.left.index = size;

					up(current.left);
	
					inserted = true;
					
				} else if(!current.hasRight()) {
					current.right = new Solutions(value, text, current);
					size += 1;
					current.right.index = size;
					
					up(current.right);
					
					inserted = true;
					
				} else {
					if(current.hasLeft()){
						queue.add(current.left);
					}
					if(current.hasRight()){
						queue.add(current.right);
					}
				}
			}
			
			return inserted;
		}else {
			if(root.quality < value) {
				root.quality = value;
				root.text = text;
				
				down(root);
				return true;
			}
		}
		return false;
	}
	
	private void up(Solutions solutions) {
		Solutions child = solutions; // child == 1
		Solutions parent = child.parent; // 1/2 = 0
		if (child.parent != null) {
			if (child.quality < parent.quality) { // element[1] == 9 > element[0] == 8
				swap(child, parent);
				up(parent); // up(0)
			}
		}
	}
	
	private void down(Solutions solutions) {
		Solutions parent = solutions;// p = 3
		Solutions childRigth = parent.right;
		Solutions childLeft = parent.left;

		
		if (parent.index < size) {									// verifica se o pai ainda tem filho
			
			if(parent.hasRight()) {								// verifica se o filho da direnta existe
				
				if(parent.hasLeft()) {							// verifica se tem filho na direita e na esquerda
					
					if (childRigth.quality <= childLeft.quality) {		// verifica se o filho da direita � maior ou igual ao da esquerda
						
						if (childRigth.quality < parent.quality) {		// verifica se o filho da direita � maior que o pai
							//swap(parent, childRigth);
							swap(childRigth, parent);
							down(childRigth);
							//down(parent);
						}
					}
						if (childLeft.quality < parent.quality) {		// verifica se o filho da esquerda � maior que o pai
							//swap(parent, childLeft);
							swap(childLeft, parent);
							down(childLeft);
							//down(parent);
						}
						
				} else {
					if(childRigth.quality < parent.quality) {			// verifica se o filho da direita � maior que o pai, j� que n�o tem filho � esquerda
					//  swap(parent, childRigth);
						swap(childRigth, parent);
						down(childRigth);
						//down(parent);
					}
				}
			} else if(parent.hasLeft()) {						// j� que n�o tem filho na direita, verifica se tem na esquerda					
				if(childLeft.quality < parent.quality) {				// verifica se o filho da esquerda � maior que o pai
				//	swap(parent, childLeft);
					swap(childLeft, parent);
					down(childLeft);
					//down(parent);
				}
			}
		} 
	}
	
	private void swap(Solutions child, Solutions parent) {
		double auxD = child.quality;
		String auxS = child.text;
		
		child.quality = parent.quality;
		child.text = parent.text;
		
		parent.quality = auxD;
		parent.text = auxS;
	}

	/*public double remove() {
		
		if(root == null) {
			//println("A �rvore n�o possui elemento!");
			return -111;
		}
		double removed = root.quality;
    	//System.out.println("------------------------------------- " + removed);
		
		root = removeLast();
    	//System.out.println("--------------------------+++----------- " + root.value);
		size--;
		down(root);
		
		return removed;
	}*/
	
	/*protected Solutions removeLast() {
		return removeLast( root, false);
	}

	private Solutions removeLast(Solutions solutions,  boolean isLeft) {
		
	    if (solutions != null) {
	    	Solutions removed, removedRight, removedLeft;
	    	
	    	if(solutions.index == size) {
	    		removed = solutions;
		    	
		    	if(solutions.parent.right == solutions) {
		    		solutions.parent.right = null;
		    	}else {
		    		solutions.parent.left = null;
		    	}
		    	
		    	return removed;
	    	}else {
	    		removedRight = removeLast(solutions.right, false);
	    		removedLeft = removeLast(solutions.left, true);
	    		if(removedLeft != null) {
	    			return removedLeft;
	    		}else {
	    			return removedRight;
	    		}
	    	}	
	    }
	   
	    return null;
	}*/

	public double get() {
		
		return root.quality;
	}
	
	protected Solutions getSolutions(double current) {
		
		
		return getSolutions(root, current);
	}
	
	protected Solutions getSolutions(Solutions solutions, double current) {
		if(solutions != null) {

			if(solutions.quality == current) {
				return solutions;
			}else {
				
				return  getSolutions(solutions.right, current) != null ? getSolutions(solutions.right, current) : getSolutions(solutions.left, current);
			}
			
		}
		
		return null;
			
	}

	public int size() {
		return size;
	}

	public void inOrder() {
		inOrder("", root,  false);
	}


	private void inOrder(String prefix, Solutions n,  boolean isLeft) {
	    if (n != null) {
	    	
	    	inOrder(prefix + "    ", n.right,  false);
	    	System.out.println(prefix + n.quality + " " + n.text);
	    	inOrder(prefix + "    ", n.left, true);
	    	
	    }
	}
	
	public Solutions recuperar() {
		Solutions solutions = root;
		
		return solutions;
	}
	
}
