package question4;

public class Main {

	public static void main(String[] args) {
		CompleteBinaryTree tree1 = new CompleteBinaryTree();

		tree1.insert(7);
		tree1.insert(5);
		tree1.insert(24);
		tree1.insert(88);
		tree1.insert(22);
		tree1.insert(33);

		int vetor[] = { 5, 22, 7, 88, 33, 24 };

		tree1.inOrder(); /// printa a árvore inicialmente. ///

		for (int i = 0; i < 6; i++) {

			tree1.remove(vetor[i]);

			System.out.println(" ------------------------------- ");
			System.out.println(" ------------------------------- ");

			tree1.inOrder();
		}

	}

}
