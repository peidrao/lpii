package question5;

public class NotAVL extends AVLTree {
	
	private int getHeight(Node n) {
		return n == null ? 0 : n.height;
	}
	
	
	protected Node checkBalance(Node node) {
		node.height = 1 + Math.max(getHeight(node.left), getHeight(node.right));
		int balance = getHeight(node.right) - getHeight(node.left);
		if (balance > 1) {
			if (getHeight(node.right.right) < getHeight(node.right.left)) {
				node.right = rotateRight(node.right);
			}
			node = rotateLeft(node);
		} else if (balance < -1) {
			if (getHeight(node.left.left) < getHeight(node.left.right)) {
				node.left = rotateLeft(node.left);
			}
			node = rotateRight(node);
		}
		return node;
	}
}
