package question2;

public class Main {

	public static void main(String[] args) {
		BinarySearchTree tree1 = new BinarySearchTree();
		BinarySearchTree tree2 = new BinarySearchTree();
        
		tree1.insert(15);
		tree1.insert(13);
		tree1.insert(12);
		tree1.insert(14);
		tree1.insert(17);
		tree1.insert(16);
		tree1.insert(18);
		tree1.insert(5);
		
		tree2.insert(1);
		tree2.insert(2);
		tree2.insert(3);
		tree2.insert(4);
	
		
		System.out.println(tree1.checkBalance());
		System.out.println(tree2.checkBalance());
	
	}

}
