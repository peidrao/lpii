package question3;

public class CompleteBinaryTree extends BinarySearchTree {

	public boolean insert(int value) {
		if (root == null) {
			root = new Node(value);
			return true;
		} else {
			return insert(root, 0, value);
		}
	}

	private boolean insert(Node node, int index, int value) {
		boolean flag = false;

		while (!flag) {
			Node current = node;

			for (int i = 0; i < index; i++) {
				current = current.left;
			}
			if (!current.hasLeft()) {
				current.left = new Node(value);
				flag = true;
			} else if (!current.hasRight()) {
				current.right = new Node(value);
				flag = true;
			} else {
				current = node;
				for (int i = 0; i < index; i++) {
					current = current.right;
				}
				if (!current.hasLeft()) {
					current.left = new Node(value);
					flag = true;
				} else if (!current.hasRight()) {
					current.right = new Node(value);
					flag = true;
				}
			}

			index++;
		}
		if (flag) {
			return true;
		}

		return false;
	}

	public void inOrder() {
		inOrder("", root, 1, false);
	}

	public void inOrder(String prefix, Node n, Integer level, boolean isLeft) {
		if (n != null) {

			inOrder(prefix + "    ", n.right, level + 1, false);
			System.out.println(prefix + n.value + " [Level:" + level + "]");
			inOrder(prefix + "    ", n.left, level + 1, true);
		}
	}

}